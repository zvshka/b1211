const {
  Command
} = require('../../handler')
const Utils = require('../../Utils')

module.exports = class extends Command {
  constructor() {
    super('reload', {
      aliases: [],
      info: 'Reload module',
      usage: 'reload [module]',
      guildOnly: false,
      permLevel: 'Bot Admin',

    })
  }
  async run(handler, message, args, level) {
    let response = await handler.unloadModule(args[0]);
    if (response) return message.reply(`Error Unloading: ${response}`);
    let file = handler.files.get(args[0])
    let type = file[1]
    switch (type) {
      case "command":
        var Node = require(file[0])
        response = handler.loadCommand(new Node(handler));
        if (response) return message.reply(`Error Loading: ${response}`);

        message.reply(`The command \`${args[0]}\` has been reloaded`);
        break;
      case "event":
      var  Node = require(file[0])
        response = handler.loadEvent(new Node(handler));
        if (response) return message.reply(`Error Loading: ${response}`);

        message.reply(`The command \`${args[0]}\` has been reloaded`);
        break;
      case "feature":
      var  Node = require(file[0])
        response = handler.loadFeature(new Node(handler));
        if (response) return message.reply(`Error Loading: ${response}`);

        message.reply(`The command \`${args[0]}\` has been reloaded`);
        break;
    }
  }
}
