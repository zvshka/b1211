const url = require('url')

class Auth {
    constructor(app, depend, passport) {
        this.passport = passport,
            this.app = app,
            this.depend = depend
    }

    static checkAuth(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.session.backURL = req.url;
        res.redirect("/login");
    }

    init() {
        this.app.get("/login", (req, res, next) => {
            if (req.session.backURL) {
                req.session.backURL = req.session.backURL;
            } else if (req.headers.referer) {
                const parsed = url.parse(req.headers.referer);
                if (parsed.hostname === this.app.locals.domain) {
                    req.session.backURL = parsed.path;
                }
            } else {
                req.session.backURL = "/";
            }
            next();
        },
            this.passport.authenticate("discord")
        );
        this.app.get("/logout", function (req, res) {
            req.session.destroy(() => {
                req.logout();
                res.redirect("/"); //Inside a callback… bulletproof!
            });
        });
        this.app.get("/callback", this.passport.authenticate("discord", { failureRedirect: "/autherror" }), (req, res) => {
            if (req.user.id === this.depend.handler.config.ownerID) {
                req.session.isAdmin = true;
            } else {
                req.session.isAdmin = false;
            }
            if (req.session.backURL) {
                const url = req.session.backURL;
                req.session.backURL = null;
                res.redirect(url);
            } else {
                res.redirect("/");
            }
        });
    }
}

module.exports = Auth