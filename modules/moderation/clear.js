const { Command } = require('../../handler')
const Utils = require('../../Utils')
const path = require('path')

module.exports = class extends Command {
    constructor() {
        super('clear', {
            aliases: [],
            info: 'clear',
            usage: 'clear [amount]/[total]',
            guildOnly: false,
            permLevel: 'User',
            category: "Moderation"
        })
    }
    run(handler, message, args, level) {
      if (!args[0]) return message.channel.send(Utils.resEmbed(`error`, 1, `Укажите кол-во`)).then(msg => {
          msg.delete(5000);
      });
      if (args[0].toLowerCase() === 'total') {
          let toclear = message.channel;
          toclear.delete();
          message.guild.createChannel(`${toclear.name}`, `text`)
              .then(chnl => {
                  chnl.setParent(toclear.parent.id);
                  chnl.setPosition(`${toclear.position}`);
                  chnl.send(Utils.resEmbed(`succes`, 0, `Канал пересоздан`)).then(msg => msg.delete(15000));
              });
      } else {
          message.channel.bulkDelete(args[0], true).then(() => {
              message.channel.send(
                  Utils.resEmbed(`succes`, 0, `Удалено ${args[0]} сообщений`)
              ).then(msg => msg.delete(5000));
          });
      }
    }
}
