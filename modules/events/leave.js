const { Event } = require('../../handler')
const Utils = require('../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Event {
    constructor() {
        super('guildMemberRemove')
        this.disable()
    }

    async run(handler, member) {
        const settings = Utils.getSettings(handler, member.guild)

        const msg = settings.leave.replace('{{user}}', member.user.tag).replace('{{time}}', Date);

        const modlog = await member.guild.channels.find(c => c.name === settings.modlog)

        if (!modlog && settings.leave.enabled === "true") {
            if (!member.guild.channels.find(c => c.name === settings.leave.channel)) return;
            member.guild.channels.find(c => c.name === settings.leave.channel).send(msg).catch(console.error)
        }

        const caseNum = await Utils.caseNum(modlog)
        const entry = await member.guild.fetchAuditLogs({ type: 20, user: member }).entries().first()
        if (!entry.executor.bot) {
            const embed = new RichEmbed()
                .setColor(0x00AE86)
                .setTimestamp()
                .setDescription(`**Действие:** Kick\n**Цель:** ${entry.target.tag}\n**Модератор:** ${entry.executor.tag}\n**Причина:** ${entry.reason || `Ожидание причины. Используй ${settings.prefix}reason ${caseNum} <reason>.`}`)
                .setFooter(`Case ${caseNum}`);
            modlog.send(embed);
        }
        if (settings.leave.enabled === "true") {
            if (!member.guild.channels.find(c => c.name === settings.leave.channel)) return;
            member.guild.channels.find(c => c.name === settings.leave.channel).send(msg).catch(console.error)
        }
    }
}
