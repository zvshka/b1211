const Strategy = require("passport-discord").Strategy;
const helmet = require('helmet')

class Passport {
    constructor(app, passport, depend) {
        this.app = app,
            this.passport = passport,
            this.depend = depend
    }
    init() {
        this.app.use(this.passport.initialize());
        this.app.use(this.passport.session());
        this.app.use(helmet());

        this.passport.serializeUser((user, done) => {
            done(null, user);
        });
        this.passport.deserializeUser((obj, done) => {
            done(null, obj);
        });
        this.passport.use(new Strategy({
            clientID: this.depend.app.user.id,
            clientSecret: this.depend.handler.config.dashboard.oauthSecret,
            callbackURL: this.depend.handler.config.dashboard.callbackURL,
            scope: ["identify", "guilds"]
        },
            (accessToken, refreshToken, profile, done) => {
                return process.nextTick(() => done(null, profile));
            }
        ));
    }
}
module.exports = Passport