const config = {
    "ownerID": "263349725099458566",
    "support": ["351040419947216907"],
    "admins": [],
    "token": process.env.TOKEN,
    "dashboard": {
        "oauthSecret": process.env.SECRET,
        "callbackURL": `http://${process.env.PROJECT_DOMAIN}/callback`,
        "sessionSecret": process.env.SESSION_SECRET,
        "domain": `${process.env.PROJECT_DOMAIN}`,
        "port": process.env.PORT
    },
    mongo: process.env.MONGO_CONNECTION,
    defaults: {
        "prefix": "?",
        "modLog": "logs",
        roles: {
            "Administrator": "Admin",
            "Moderator": "Moderator",
            "Helper": "Helper"
        },
        "systemNotice": "true",
        welcome: {
            "channel": "welcome",
            "message": "Say hello to {{user}}, everyone! We all need a warm welcome sometimes :D",
            "enabled": "true",
        },
        leave: {
            "enabled": "true",
            "message": "{{user}} leaved at `{{time}}`",
            "channel": "welcome",
        },
        /*muteOrLock: {
            "muteRole": "muted",
            "lockRole": "lock",
            "lockChannel": "locked",
        },*/

    }
}

module.exports = config
