const { Event } = require('../../../../handler')
const Utils = require('../../../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Event {
    constructor(caseNum) {
        super('guildMemberRemove')
        this.caseNum = caseNum
    }

    async run(handler, guild, member) {
        const settings = Utils.getSettings(handler, guild)
        const modlog = await member.guild.channels.find(c => c.name === settings.modlog)
        if (settings.systemNotice !== "true") return;
        const caseNum = await this.caseNum(handler.app, modlog)
        const entry = await member.guild.fetchAuditLogs({ type: 20, user: member }).entries().first()
        if (!entry.executor.bot) {
            const embed = new RichEmbed()
                .setColor(0x00AE86)
                .setTimestamp()
                .setDescription(`**Действие:** Kick\n**Цель:** ${entry.target.tag}\n**Модератор:** ${entry.executor.tag}\n**Причина:** ${entry.reason || `Ожидание причины. Используй ${settings.prefix}reason ${caseNum} <reason>.`}`)
                .setFooter(`Case ${caseNum}`);
            modlog.send(embed);
        }
    }
}
