const { Command } = require('../../../../handler')
const Utils = require('../../../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Command {
    constructor() {
        super('reason', {
            aliases: [],
            info: 'Set reason of unset case',
            usage: 'reason <id of case> <reason>',
            guildOnly: false,
            permLevel: 'User',
            category: "Moderation"
        })
    }
    async run(handler, message, args, level) {
        const settings = Utils.getSettings(handler, message.guild)
        const modlog = message.guild.channels.find(c => c.name == settings.modLog);
        if (!modlog) return message.channel.send(Utils.resEmbed('error', 1, `**Нету канала для логов**`));
        const caseNumber = args.shift();
        const newReason = args.join(' ');

        await modlog.fetchMessages({
            limit: 100
        }).then((messages) => {
            const caseLog = messages.filter(m => m.author.id === handler.client.user.id &&
                m.embeds[0] &&
                m.embeds[0].type === 'rich' &&
                m.embeds[0].footer &&
                m.embeds[0].footer.text.startsWith('Case') &&
                m.embeds[0].footer.text === `Case ${caseNumber}`
            ).first();
            modlog.fetchMessage(caseLog.id).then(logMsg => {
                const embed = logMsg.embeds[0];
                Utils.clearEmbed(embed);
                embed.description = embed.description.replace(`Ожидание причины. Используй ${settings.prefix}reason ${caseNumber} <reason>.`, newReason);
                return logMsg.edit({
                    embed
                });
            });
        });
    }
}
