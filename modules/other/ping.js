const { Command } = require('../../handler')
const Utils = require('../../Utils')

module.exports = class extends Command {
    constructor() {
        super('ping', {
            aliases: [],
            info: "ping",
            usage: "ping",
            guildOnly: false,
            category: "Other",
            permLevel: "User"
        })
    }
    async run(handler, message, args, level) {
      const msg = await message.channel.send("Ping?");
      msg.edit(`Pong! Latency is ${msg.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(app.ping)}ms`);
    }
}
