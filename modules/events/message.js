const {
  Event
} = require('../../handler');
const Utils = require('../../Utils')

module.exports = class extends Event {
  constructor() {
    super('message')
  }

  async run(depend, message) {
    if (message.author.bot) return;
    const handler = depend.handler
    let settings;
    if (message.channel.type != 'dm') {
      settings = message.settings = Utils.getSettings(handler, message.guild);
    } else {
      settings = message.settings = handler.config.defaultSettings;
    }
    const prefixMention = new RegExp(`^<@!?${handler.client.user.id}>( |)$`);
    if (message.content.match(prefixMention)) {
      return message.reply(`Мой префикс \`${settings.prefix}\``);
    }

    if (!message.content.startsWith(settings.prefix)) return;

    const args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    let cmd = handler.commands.get(command) || handler.aliases.get(command)
    if (!cmd) return;
    if (message.guild && !message.member) await message.guild.fetchMember(message.author)
    const level = Utils.level(handler, settings, message)
    if (cmd && !message.guild && cmd.guildOnly)
      return message.channel.send("Недоступно в лс.");
    if (level < handler.levelCache[cmd.permLevel]) {
      if (settings.systemNotice === "true") {
        return message.channel.send(Utils.resEmbed(`error`, 1, `Нет прав\nТвой уровень прав ${level} (${hadler.levels.find(l => l.level === level).name})\nНужен ${handler.levelCache[cmd.permLevel]} (${cmd.permLevel}))`, 0x36393f));
      } else {
        return;
      }
    }
    message.author.permLevel = level;

    message.flags = [];
    while (args[0] && args[0][0] === "-") {
      message.flags.push(args.shift().slice(1));
    }

    if (!cmd.isEnabled) return message.channel.send(Utils.resEmbed(`error`, 1, `Команда выключена`)).catch(console.error);

    cmd.run(handler, message, args, level)
  }
}
