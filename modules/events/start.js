const { Event } = require('../../handler');
const Dashboard = require('../../handler/Dashboard.js')

module.exports = class extends Event {
  constructor() {
    super('ready');
  }

  async run(depend) {
    const handler = depend.handler
    const client = depend.app
    if (!handler.settings.has("default")) {
      await handler.settings.set("default", handler.config.defaults);
    }

    handler.handle(`${client.user.tag} starting`, "ready")
    client.user.setPresence({ game: { name: `${handler.settings.get("default").prefix}help | ${client.guilds.size} Servers| Dashboard: https://${handler.config.dashboard.domain}.glitch.me`, type: 0 } })
    for (let i = 0; i < handler.levels.length; i++) {
      const thisLevel = handler.levels[i];
      handler.levelCache.set(thisLevel.name, thisLevel.level)
    }

    client.appInfo = await client.fetchApplication();
    setInterval(async () => {
      client.appInfo = await client.fetchApplication();
    }, 60000);
    new Dashboard(depend).init()
    String.prototype.toProperCase = function () {
      return this.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    };
  }
};
