const { Feature } = require('../../../handler')
const BanEvent = require('./events/ban')
const BanCommand = require('./commands/ban')
const KickEvent = require('./events/kickEvnt')
const KickCommand = require('./commands/kickCmd')

module.exports = class extends Feature {
    constructor() {
        super('withCases')
        this.registerCommand(new BanCommand(this.caseNum))
        this.registerCommand(new KickCommand(this.caseNum))
        this.registerEvent(new KickEvent(this.caseNum))
        this.registerEvent(new BanEvent(this.caseNum))
    }

    async caseNum(app, channel) {
        const messages = await channel.fetchMessages({
            limit: 15
        });
        const log = messages.filter(m => m.author.id === app.user.id &&
            m.embeds[0] &&
            m.embeds[0].type === "rich" &&
            m.embeds.footer &&
            m.embeds[0].footer.text.startsWith("Case")
        ).first()
        if (!log) return 1
        const thisCase = /Case\s(\d+)/.exec(log.embeds[0].footer.text);
        return thisCase ? parseInt(thisCase[1]) + 1 : 1;
    }
}
