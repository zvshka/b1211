const { Event } = require('../../handler')
const Utils = require('../../Utils')

module.exports = class extends Event {
    constructor() {
        super('guildMemberAdd')
    }

    run(handler, member) {
        const settings = Utils.getSettings(handler, member.guild)

        if (settings.welcome.enabled !== "true") return;
        if (!member.guild.channels.find(c => c.name === settings.welcome.channel)) return;

        const msg = settings.welcome.message.replace("{{user}}", member.user.tag).replace("{{time}}", Date)

        member.guild.channels.find(c => c.name === settings.welcome.channel).send(msg).catch(console.error)
    }
}
