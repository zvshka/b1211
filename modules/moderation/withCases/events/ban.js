const { Event } = require('../../../../handler')
const Utils = require('../../../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Event {
    constructor(caseNumber) {
        super('guildBanAdd')

        this.caseNumber = caseNumber
    }

    async run(handler, guild, user) {
        const settings = Utils.getSettings(handler, guild)

        const modLog = guild.channels.find(c => c.name === settings.modLog)
        if (settings.systemNotice !== "true") return;
        const caseNum = await this.caseNumber(handler.app, guild)

        const entry = await guild.fetchAuditLogs({ type: "MEMBER_BAN_ADD", user: user }).entries().first()

        if (!entry.executor.bot && modLog) {
            const embed = new RichEmbed()
                .setColor(0x00AE86)
                .setTimestamp()
                .setDescription(`**Действие:** Ban\n**Цель:** ${entry.target.tag}\n**Модератор:** ${entry.executor.tag}\n**Причина:** ${entry.reason || `Ожидание причины. Используй ${settings.prefix || defSettings.prefix}reason ${caseNum} <reason>.`}`)
                .setFooter(`Case ${caseNum}`);
            modlog.send(embed);
        }
    }
}
