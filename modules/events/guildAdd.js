const Utils = require('../../Utils')

const { Event } = require('../../handler')

module.exports = class extends Event {
    constructor() {
        super('guildCreate')
    }

    run(depend, guild) {
        const handler = depend.handler
        handler.client.setPresence({ game: { name: `${handler.settings.get("default").prefix}help | ${client.guilds.size} Servers| Dashboard: https://${handler.config.dashboard.domain}.glitch.me`, type: 0 } })
    }
}
