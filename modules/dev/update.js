const { Command } = require('../../handler')
const Utils = require('../../Utils')

module.exports = class extends Command {
    constructor() {
        super('update', {
            aliases: ['u', 'upd'],
            info: 'update bot',
            usage: 'update',
            guildOnly: false,
            permLevel: 'Bot Owner',

        })
    }

    run(handler, message, args, level) {

    }
}
