const { Command } = require('../../../../handler')
const Utils = require('../../../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Command {
    constructor(caseNum) {
        super('kick', {
            aliases: [],
            info: "Kick somebody",
            usage: "kick [@member] [reason]",
            guildOnly: true,
            permLevel: "Moderator",
            category: "Moderation"
        })
        this.caseNum = caseNum
    }

    async run(handler, message, args, level) {
        const settings = await Utils.getSettings(handler, message.guild)
        if (message.mentions.users.size < 1)
            return message.channel
                .send(Utils.resEmed("error", 1, "**Не указан пользователь**"))
                .catch(console.error);
        const bmember = message.guild.member(message.mentions.users.first());
        if (bmember.highestRole.position >= message.guild.me.highestRole.position)
            return message.channel.send(
                Utils.resEmbed("error", 2, "**User выше меня по роли**")
            );
        if (bmember.highestRole.position >= message.member.highestRole.position) {
            return message.channel.send(
                Utils.resEmbed("error", 2, "**User выше тебя по роли**")
            );
        }
        let modlog = message.guild.channels.find(
            c => c.name === settings.modLog
        );
        if (!modlog) {
            return message.channel.send(
                Utils.resEmbed("error", 1, "**Нету канала для логов**")
            );
        };
        const caseNum = await this.caseNum(handler.client, message.channel)
        const reason =
            args.splice(1, args.length).join(" ") ||
            `Ожидание причины. Используй ${settings.prefix}reason ${caseNum} <reason>.`;
        bmember.kick()
        const embed = new RichEmbed()
            .setColor(0x00ae86)
            .setTimestamp()
            .setDescription(
                `**Действие:** Kick\n**Цель:** ${bmember.user.tag}\n**Модератор:** ${
                message.author.tag
                }\n**Причина:** ${reason}`
            )
            .setFooter(`Case ${caseNum}`);
        return modlog.send({
            embed
        });
    }
}
