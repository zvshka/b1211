const { Command } = require('../../handler')
const Utils = require('../../Utils')
const {
  version,
  RichEmbed
} = require("discord.js");
const moment = require("moment");
require("moment-duration-format");

module.exports = class extends Command {
    constructor() {
        super('stats', {
            aliases: [],
            info: "bot stats",
            usage: "stats",
            guildOnly: false,
            category: "Other",
            permLevel: "User"
        })
    }
    run(handler, message, args, level) {
        const duration = moment
          .duration(hadler.client.uptime)
          .format(" D [days], H [hrs], m [mins], s [secs]");

        const embed = new RichEmbed()
          .setColor(0x36393f)
          .addField(
            `**Invite**`,
            `[Invite me](https://discordapp.com/oauth2/authorize?&client_id=${
              handler.client.user.id
            }&scope=bot&permissions=8)`
          )
          .addField(
            `**Mem Usage/ Mem**`,
            `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB / 512 MB`
          )
          .addField(`**Uptime**`, `${duration}`)
          .addField(
            `**Cpu/Cores**`,
            `${require("os").cpus()[0].model} / ${require("os").cpus().length}`
          )
          .addField(
            `**Servers/Users/Channels**`,
            `${handler.client.users.size.toLocaleString()} / ${handler.client.guilds.size.toLocaleString()} / ${handler.client.channels.size.toLocaleString()}`
          )
          .addField(`**Versions: D.js/Node.js**`, `v${version} / ${process.version}`);

        message.channel.send(embed);
    }
}
