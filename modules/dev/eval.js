const { Command } = require('../../handler')
const Utils = require('../../Utils')
const Discord = require('discord.js')
const { execSync } = require('child_process');

module.exports = class extends Command {
    constructor() {
        super('eval', {
            aliases: [],
            info: 'eval',
            usage: 'eval [code]',
            guildOnly: false,
            permLevel: 'Bot Owner',
            category: "Dev",
        })
    }
    run(handler, message, args, level) {
        try {
            let codein = args.join(" ");
            let code = eval(codein);

            if (typeof code !== 'string')
                code = require('util').inspect(code, { depth: 0 });
            let embed = new Discord.RichEmbed()
                .setAuthor('Evaluate')
                .setColor('RANDOM')
                .addField(':inbox_tray: Input', `\`\`\`js\n${codein}\`\`\``)
                .addField(':outbox_tray: Output', `\`\`\`js\n${code}\n\`\`\``)
            message.channel.send(embed)
        } catch (e) {
            message.channel.send(`\`\`\`js\n${e}\n\`\`\``);
        }
    }
}
