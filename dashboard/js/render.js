const moment = require('moment')
require('moment-duration-format')
const Discord = require('discord.js')
const Auth = require('./auth')
const Utils = require('../../Utils')
const path = require('path')
const dataDir = path.resolve(`${process.cwd()}${path.sep}dashboard`);
const md = require('marked')

// This resolves to: yourbotdir/dashboard/templates/
// which is the folder that stores all the internal template files.
const templateDir = path.resolve(`${dataDir}${path.sep}templates`);

class Render {
  constructor(app, depend) {
    this.app = app,
      this.depend = depend
  }
  render(res, req, template, data = {}) {
    const baseData = {
      bot: this.depend.app,
      path: req.path,
      user: req.isAuthenticated() ? req.user : null,
      utils: Utils,
      handler: this.depend.handler
    };
    res.render(path.resolve(`${templateDir}${path.sep}${template}`), Object.assign(baseData, data));
  }
  init() {
    const client = this.depend.app
    this.app.get("/autherror", (req, res) => {
      this.render(res, req, "autherror.ejs");
    });
    this.app.get("/", (req, res) => {
      this.render(res, req, "index.ejs")
    })
    this.app.get("/commands", (req, res) => {
      this.render(res, req, "commands.ejs", {
        md
      });
    });
    this.app.get("/stats", (req, res) => {
      const duration = moment.duration(client.uptime).format(" D [days], H [hrs], m [mins], s [secs]");
      const members = client.guilds.reduce((p, c) => p + c.memberCount, 0);
      const textChannels = client.channels.filter(c => c.type === "text").size;
      const voiceChannels = client.channels.filter(c => c.type === "voice").size;
      const guilds = client.guilds.size;
      this.render(res, req, "stats.ejs", {
        stats: {
          servers: guilds,
          members: members,
          text: textChannels,
          voice: voiceChannels,
          uptime: duration,
          memoryUsage: (process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2),
          dVersion: Discord.version,
          nVersion: process.version
        }
      });
    });
    this.app.get("/dashboard", Auth.checkAuth, (req, res) => {
      const perms = Discord.EvaluatedPermissions;
      this.render(res, req, "dashboard.ejs", {
        perms
      });
    });
    this.app.get("/admin", Auth.checkAuth, (req, res) => {
      if (!req.session.isAdmin) return res.redirect("/");
      this.render(res, req, "admin.ejs");
    });
    this.app.get("/dashboard/:guildID", Auth.checkAuth, (req, res) => {
      res.redirect(`/dashboard/${req.params.guildID}/manage`);
    });
    this.app.get("/dashboard/:guildID/manage", Auth.checkAuth, (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      const isManaged = guild && !!guild.member(req.user.id) ? guild.member(req.user.id).permissions.has("MANAGE_GUILD") : false;
      if (!isManaged && !req.session.isAdmin) res.redirect("/");
      this.render(res, req, "guild/manage.ejs", {
        guild
      });
    });
    this.app.get("/dashboard/:guildID/members", Auth.checkAuth, async (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      this.render(res, req, "guild/members.ejs", {
        guild: guild,
        members: guild.members.array()
      });
    });
    this.app.get("/dashboard/:guildID/members/list", Auth.checkAuth, async (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      if (req.query.fetch) {
        await guild.fetchMembers();
      }
      const totals = guild.members.size;
      const start = parseInt(req.query.start, 10) || 0;
      const limit = parseInt(req.query.limit, 10) || 50;
      let members = guild.members;

      if (req.query.filter && req.query.filter !== "null") {
        //if (!req.query.filtervalue) return res.status(400);
        members = members.filter(m => {
          m = req.query.filterUser ? m.user : m;
          return m["displayName"].toLowerCase().includes(req.query.filter.toLowerCase());
        });
      }

      if (req.query.sortby) {
        members = members.sort((a, b) => a[req.query.sortby] > b[req.query.sortby]);
      }
      const memberArray = members.array().slice(start, start + limit);

      const returnObject = [];
      for (let i = 0; i < memberArray.length; i++) {
        const m = memberArray[i];
        returnObject.push({
          id: m.id,
          status: m.user.presence.status,
          bot: m.user.bot,
          username: m.user.username,
          displayName: m.displayName,
          tag: m.user.tag,
          discriminator: m.user.discriminator,
          joinedAt: m.joinedTimestamp,
          createdAt: m.user.createdTimestamp,
          highestRole: {
            hexColor: m.highestRole.hexColor
          },
          memberFor: moment.duration(Date.now() - m.joinedAt).format(" D [days], H [hrs], m [mins], s [secs]"),
          roles: m.roles.map(r => ({
            name: r.name,
            id: r.id,
            hexColor: r.hexColor
          }))
        });
      }
      res.json({
        total: totals,
        page: (start / limit) + 1,
        pageof: Math.ceil(members.size / limit),
        members: returnObject
      });
    });
    this.app.get("/dashboard/:guildID/stats", Auth.checkAuth, (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      const isManaged = guild && !!guild.member(req.user.id) ? guild.member(req.user.id).permissions.has("MANAGE_GUILD") : false;
      if (!isManaged && !req.session.isAdmin) res.redirect("/");
      this.render(res, req, "guild/stats.ejs", {
        guild
      });
    });
    this.app.get("/dashboard/:guildID/leave", Auth.checkAuth, async (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      const isManaged = guild && !!guild.member(req.user.id) ? guild.member(req.user.id).permissions.has("MANAGE_GUILD") : false;
      if (!isManaged && !req.session.isAdmin) res.redirect("/");
      await guild.leave();
      res.redirect("/dashboard");
    });
    // Resets the guild's settings to the defaults, by simply deleting them.
    this.app.get("/dashboard/:guildID/reset", Auth.checkAuth, async (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      const isManaged = guild && !!guild.member(req.user.id) ? guild.member(req.user.id).permissions.has("MANAGE_GUILD") : false;
      if (!isManaged && !req.session.isAdmin) res.redirect("/");
      this.depend.handler.settings.delete(guild.id);
      res.redirect("/dashboard/" + req.params.guildID);
    });
    this.app.post("/dashboard/:guildID/manage", Auth.checkAuth, (req, res) => {
      const guild = client.guilds.get(req.params.guildID);
      if (!guild) return res.status(404);
      const isManaged = guild && !!guild.member(req.user.id) ? guild.member(req.user.id).permissions.has("MANAGE_GUILD") : false;
      if (!isManaged && !req.session.isAdmin) res.redirect("/");
      Utils.writeSettings(this.depend.handler, guild.id, req, Utils.getSettings(this.depend.handler, guild));
      res.redirect("/dashboard/" + req.params.guildID + "/manage");
    });
  }
}

module.exports = Render
