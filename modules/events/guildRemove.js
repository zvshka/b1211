const { Event } = require('../../handler')
const Utils = require('../../Utils')

module.exports = class extends Event {
    constructor() {
        super('guildDelete')
    }

    run(depend, guild) {
        const handler = depend.commanHandler
        handler.app.setPresence({ game: { name: `${handler.settings.get("default").prefix}help | ${handler.app.guilds.size} Servers| Dashboard soon!`, type: 0 } })
        handler.settings.delete(guild.id)
    }
}
