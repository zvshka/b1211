const { Command } = require('../../handler')
const Utils = require('../../Utils')

module.exports = class extends Command {
    constructor() {
        super('reboot', {
            aliases: [],
            info: 'Reboot Bot',
            usage: 'Reboot',
            guildOnly: false,
            permLevel: 'User',
            category: "Dev",
        })
    }
    async run(handler, message, args, level) {
      await message.reply("Bot is shutting down.");
      handler.commands.forEach( async cmd => {
        await handler.unloadCommand(cmd);
      });
      process.exit(1);
    }
}
