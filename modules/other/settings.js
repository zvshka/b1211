const { Command } = require('../../handler');
const { inspect } = require("util");
const { RichEmbed } = require('discord.js')

const Utils = require('../../Utils')

module.exports = class extends Command {
    constructor() {
        super('settings', {
            aliases: ['s', "set", "conf"],
            usage: "settings [edit/get/reset/del] [key]",
            info: "settings system",
            permLevel: "Server Owner",
            category: "Moderation"
        })
    }

    async run(handler, message, [action, key, ...value], level) {
        const settings = Utils.getSettings(handler, message.guild)
        const defaults = handler.settings.get("default")
        if (action === "edit") {
            if (!key) return message.channel.send(Utils.resEmbed("error", 1, "Вы не указали ключ"));
            if (key.split('.')[0] && key.split('.')[1]) {
                if (!settings[key.split('.')[0]]) return message.channel.send(Utils.resEmbed("error", 1, "Такого первичного ключа не существует"));
                if (!settings[key.split('.')[0]][key.split('.')[1]]) return message.channel.send(Utils.resEmbed("error", 1, "Такого вторичного ключа не существует"));
                settings[key.split('.')[0]][key.split('.')[1]] = value.join(' ')
                handler.settings.set(message.guild.id, settings)
                return message.channel.send(Utils.resEmbed("succes", 0, `${key} успешно изменён`))
            }
            if (!settings[key]) return message.channel.send(Utils.resEmbed("error", 1, "Такого ключа не существует"));
            if (value.length < 1) return message.channel.send(Utils.resEmbed("error", 1, "Укажите значение"))

            settings[key] = value.join(' ');

            handler.settings.set(message.guild.id, settings)
            message.channel.send(Utils.resEmbed("succes", 0, `${key} успешно изменён`))
        } else if (action === "del" || action === "reset") {
            if (!key) return message.channel.send(Utils.resEmbed("error", 1, "Укажите ключ"))
            if ((key.split('.')[0] && key.split('.')[1])) {
                if (!settings[key.split('.')[0]]) return message.channel.send(Utils.resEmbed("error", 1, "Такого первичного ключа не существует"));
                if (!settings[key.split('.')[0]][key.split('.')[1]]) return message.channel.send(Utils.resEmbed("error", 1, "Такого вторичного ключа не существует"));
                const response = await Utils.awaitReply(message, "Вы уверенны?")

                if (["y", "yes", "да"].includes(response.toLowerCase())) {
                    delete settings[key.split('.')[0]][key.split('.')[1]]
                    handler.settings.set(message.guild.id, settings)
                    return message.channel.send(Utils.resEmbed("succes", 0, `${key} успешно сброшен`))
                }

                if (["n", "no", "нет"].includes(response.toLowerCase())) {
                    return message.channel.send(Utils.resEmbed("succes", 0, `Успешно отменено`))
                }
            }
            if (!settings[key]) return message.channel.send(Utils.resEmbed("error", 1, "Такого ключа не существует"));
            const response = await Utils.awaitReply(message, "Вы уверенны?")

            if (["y", "yes", "да"].includes(response.toLowerCase())) {
                delete settings[key]
                handler.settings.set(message.guild.id, settings)
                return message.channel.send(Utils.resEmbed("succes", 0, `${key} успешно сброшен`))
            }

            if (["n", "no", "нет"].includes(response.toLowerCase())) {
                return message.channel.send(Utils.resEmbed("succes", 0, `Успешно отменено`))
            }
        } else if (action === "get") {
            if (!key) return message.channel.send(Utils.resEmbed("error", 1, "Вы не указали ключ"));

            if (key.split('.')[0] && key.split('.')[1]) {
                if (!settings[key.split('.')[0]]) return message.channel.send(Utils.resEmbed("error", 1, "Такого первичного ключа не существует"));
                if (!settings[key.split('.')[0]][key.split('.')[1]]) return message.channel.send(Utils.resEmbed("error", 1, "Такого вторичного ключа не существует"));
                return message.channel.send(Utils.resEmbed("succes", 0, `Текущее состояние **${key}**: ${settings[key.split('.')[0]][key.split('.')[1]]}`))
            }
            return message.channel.send(Utils.resEmbed("succes", 0, `Текущее состояние **${key}**: ${settings[key]}`))
        } else {
            const embed = new RichEmbed()
                .setColor(0x36393f)
            for (let setting in settings) {
                if (typeof settings[setting] == "object") {
                    for (let set in settings[setting]) {
                        embed.addField(`${setting}.${set}`, `${settings[setting][set]}`)
                    }
                } else {
                    embed.addField(`${setting}`, `${settings[setting]}`, true)
                }
            }
            message.channel.send(embed)
        }
    }
}
