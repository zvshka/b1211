const { Command } = require('../../handler')
const Utils = require('../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Command {
    constructor() {
        super('mylevel', {
            aliases: [],
            info: 'Your perm Level',
            usage: 'mylevel',
            guildOnly: false,
            permLevel: 'User',
            category: "Fun"

        })
    }
    run(handler, message, args, level) {
        const embed = new RichEmbed()
            .setColor(0x36393f)
            .addField(`**PERM LEVEL**`, `${level.name}`)
    }
}
