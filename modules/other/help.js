const { Command } = require('../../handler');
const Utils = require('../../Utils')
const { RichEmbed } = require('discord.js')

module.exports = class extends Command {
  constructor() {
    super('help', {
      aliases: ['halp', 'h'],
      info: `Showing all commands`,
      usage: 'help',
      permLevel: 0,
      category: "Other"
    });
  }

  run(handler, message, args, level) {

    const settings = Utils.getSettings(handler, message.guild)
    const myCommands = message.guild
      ? handler.commands.filter(
        cmd => handler.levelCache.get(cmd.permLevel) <= level
      )
      : handler.commands.filter(
        cmd =>
          handler.levelChache.get(cmd.permLevel) <= level &&
          cmd.guildOnly !== true
      );
    let categories = []

    myCommands.forEach(command => {
      if (!categories.includes(command.category)) {
        categories.push(command.category)
      }
    })

    categories.forEach(async cat => {
      let commands = await myCommands.filter(cmd => cmd.category === cat)
      const embed = new RichEmbed()
        .setTitle(`${cat}`.toProperCase())
      commands.forEach(cmd => {
        embed.addField(`${settings.prefix}${cmd.usage}`.toProperCase(), `${cmd.info}`.toProperCase())
      })
      embed.setColor(0xffff00)
      await message.channel.send(embed)
    })

  }
};
