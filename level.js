const level = {
  levels: [{
    level: 0,
    name: 'User',
    check: () => true,
  },
  {
    level: 2,
    name: 'Moderator',
    check: (settings, option) => {
      if (option.content) {
        try {
          const modRole = option.guild.roles.find(r => r.name.toLowerCase() === settings.Moderator.toLowerCase());
          if (modRole && option.member.roles.has(modRole.id)) return true;
        } catch (e) {
          return false;
        }
      } else {
        try {
          const modRole = option.guild.roles.find(r => r.name.toLowerCase() === settings.Moderator.toLowerCase());
          if (modRole && option.roles.has(modRole.id)) return true;
        } catch (e) {
          return false;
        }
      }
    },
  },
  {
    level: 3,
    name: 'Administrator',
    check: (settings, option) => {
      if (option.content) {
        try {
          const adminRole = option.guild.roles.find(r => r.name.toLowerCase() === settings.Administrator.toLowerCase());
          if (adminRole && option.member.roles.has(adminRole.id)) return true;
        } catch (e) {
          return false;
        }
      } else {
        try {
          const adminRole = option.guild.roles.find(r => r.name.toLowerCase() === settings.Administrator.toLowerCase());
          if (adminRole && option.roles.has(adminRole.id)) return true;
        } catch (e) {
          return false;
        }
      }
    },
  },
  {
    level: 4,
    name: 'Server Owner',
    check: (settings, option) => {
      if (option.content) {
        return option.channel.type === 'text' ? (option.guild.owner.user.id === option.author.id) : false;
      }
      return option.guild.owner.user.id === option.user.id;

    },
  },
  {
    level: 8,
    name: 'Bot Support',
    check: (settings, option, handler) => {
      if (option.content) {
        return handler.config.support.includes(option.author.id);
      }
      return handler.config.support.includes(option.user.id);

    },
  },
  {
    level: 9,
    name: 'Bot Admin',
    check: (settings, option, handler) => {
      if (option.content) {
        return handler.config.admins.includes(option.author.id);
      }
      return handler.config.admins.includes(option.user.id);

    },
  },
  {
    level: 10,
    name: 'Bot Owner',
    check: (settings, option, handler) => {
      if (option.content) {
        return handler.config.ownerID === option.author.id;
      }
      return handler.config.ownerID === option.user.id;

    },
  },
  ],
};

module.exports = level;
