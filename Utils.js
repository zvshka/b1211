const fs = require('fs');
const path = require('path');
const {
  RichEmbed,
} = require('discord.js');

class Utils {
  /**
   * @description Read a directory recursively to get all files
   * @param {string} directory The directory to read
   * @returns {Array<string>} All the paths to the files
   */
  static readdirSyncRecursive(directory, savePath) {
    if (!savePath) {
      let files = [];

      fs.readdirSync(directory)
        .forEach((file) => {
          const location = path.join(directory, file);

          // If the file is a directory read it recursively
          if (fs.lstatSync(location).isDirectory()) {
            files = files.concat(Utils.readdirSyncRecursive(location));
          } else {
            files.push(location);
          }
        });

      return files;
    }
    let files = [];

    fs.readdirSync(directory)
      .forEach((file) => {
        const location = path.join(directory, file);

        // If the file is a directory read it recursively
        if (fs.lstatSync(location).isDirectory()) {
          files = files.concat(Utils.readdirSyncRecursive(location));
        } else {
          files.push(location);
        }
      });

    return files;

  }

  static getSettings(handler, guild) {
    if (!guild) return handler.settings.get('default');
    const guildConf = handler.settings.get(guild.id) || {};
    return {
      ...handler.settings.get('default'),
      ...guildConf,
    };
  }

  static level(handler, settings, message, member) {
    if (!member) {
      let permlvl = 0;

      const permOrder = handler.levels
        .slice(0)
        .sort((p, c) => (p.level < c.level ? 1 : -1));

      while (permOrder.length) {
        const currentLevel = permOrder.shift();
        if (message.guild && currentLevel.guildOnly) continue;
        if (currentLevel.check(settings, message, handler)) {
          permlvl = currentLevel.level;
          break;
        }
      }
      return permlvl;
    }
    let permlvl = 0;

    const permOrder = handler.levels
      .slice(0)
      .sort((p, c) => (p.level < c.level ? 1 : -1));

    while (permOrder.length) {
      const currentLevel = permOrder.shift();
      if (member.guild && currentLevel.guildOnly) continue;
      if (currentLevel.check(settings, member, handler)) {
        permlvl = currentLevel.level;
        break;
      }
    }
    return permlvl;

  }

  /**
   *
   * @param {string} type type of response
   * @param {number} level level of err if err
   * @param {string[]} value value of response
   */
  static resEmbed(type, level, ...value) {
    const embed = new RichEmbed();
    switch (type.toLowerCase()) {
      case 'succes':
        embed
          .addField(
            '**SUCCES**',
            `Действие выполнено успешно: ${value.toString()}`,
          )
          .setColor(0x00ff00);
        return embed;
      case 'error':
        switch (level) {
          case 1:
            embed
              .addField(
                '**ERROR**',
                `При действии возникла лёгкая ошибка: ${value.toString()}`,
              )
              .setColor(0xffff00);
            return embed;
          case 2:
            embed
              .addField(
                '**ERROR**',
                `При действии возникла средняя ошибка: ${value.toString()}`,
              )
              .setColor(0xff3300);
            return embed;
          case 3:
            embed
              .addField(
                '**ERROR**',
                `При действии возникла серьёзная ошибка: ${value.toString()}`,
              )
              .setColor(0xff0000);
            return embed;
        }
      case 'denie':
        embed.addField('**DENIE**', 'Отказанно в доступе').setColor(0xff0000)
          .addField('RESPONSE', `${value}`);
        return embed;
    }
  }

  static async awaitReply(message, question, limit = 60000) {
    const filter = m => (m.author.id == message.author.id);
    await message.channel.send(question);
    try {
      const collected = await message.channel.awaitMessages(filter, {
        max: 1,
        time: limit,
        errors: ['time'],
      });
      return collected.first().content;
    } catch (e) {
      return false;
    }
  }
  /**
   *
   * @param {Client} app discord.js client
   * @param {Channel} channel channel to scan
   */

  static async clearEmbed(toClear) {
    toClear.message ? delete toClear.message : null;
    toClear.footer ? delete toClear.footer.embed : null;
    toClear.provider ? delete toClear.provider.embed : null;
    toClear.thumbnail ? delete toClear.thumbnail.embed : null;
    toClear.image ? delete toClear.image.embed : null;
    toClear.author ? delete toClear.author.embed : null;
    toClear.fields
      ? toClear.fields.forEach((f) => {
        delete f.embed;
      })
      : null;
    return toClear;
  }

  static writeSettings(handler, id, req, settings) {
    // Using the spread operator again, and lodash's "pickby" function to remove any key
    // from the settings that aren't in the defaults (meaning, they don't belong there)
    for (const key in req.body) {
      if (req.body[key] == 'Please Select') return;
      if (key.split('.')[0] && key.split('.')[1]) {
        settings[key.split('.')[0]][key.split('.')[1]] = req.body[key];
        handler.settings.set(id, settings);
      } else {
        settings[key] = req.body[key];

        handler.settings.set(id, settings);
      }
    }
  }

  static getPath(name) {
    //    return path.join(path.dirname(require.main.filename), this.readdirSyncRecursive(path.dirname(require.main.filename))[name].path)
  }
}

module.exports = Utils;
